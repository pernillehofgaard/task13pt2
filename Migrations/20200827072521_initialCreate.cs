﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task13Pt2.Migrations
{
    public partial class initialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    POB = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    Director = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    ActorId = table.Column<int>(nullable: false),
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.ActorId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Actor_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actor",
                columns: new[] { "Id", "DOB", "FirstName", "Gender", "LastName", "MiddleName", "POB", "Pic" },
                values: new object[,]
                {
                    { 1, new DateTime(1970, 4, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Uma", "Female", "Thurman", null, "USA", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.biography.com%2Factor%2Fuma-thurman&psig=AOvVaw01riMZpjNu7u2mv8DSzeS7&ust=1598516849344000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPjAm7u5uOsCFQAAAAAdAAAAABAD" },
                    { 2, new DateTime(1965, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Robert", "Male", "Downey", "Jr", "USA", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FRobert_Downey_Jr.&psig=AOvVaw0Y4ge6_JG2R8Zwtp2f8Df9&ust=1598516998212000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPDJsIK6uOsCFQAAAAAdAAAAABAE" },
                    { 3, new DateTime(1996, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tom", "Male", "Holland", null, "England", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.biography.com%2Factor%2Ftom-holland&psig=AOvVaw1fQ9Vsq2Vs9jyStT_GQg-Y&ust=1598517074586000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIjLwaW6uOsCFQAAAAAdAAAAABAD" },
                    { 4, new DateTime(1954, 2, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", "Male", "Travolta", null, "USA", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fb%2Fb7%2FJohn_Travolta_Deauville_2013_2.jpg&imgrefurl=https%3A%2F%2Fno.wikipedia.org%2Fwiki%2FJohn_Travolta&tbnid=12x4AimdIxak5M&vet=12ahUKEwig0M3AurjrAhVyxosKHfDPDG0QMygEegUIARDYAQ..i&docid=evrgPxa9ll8bJM&w=509&h=720&q=john%20travolta&ved=2ahUKEwig0M3AurjrAhVyxosKHfDPDG0QMygEegUIARDYAQ" },
                    { 5, new DateTime(1948, 12, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Samuel", "Male", "Jackson", "Leroy", "USA", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.britannica.com%2F77%2F191077-050-63262B99%2FSamuel-L-Jackson.jpg&imgrefurl=https%3A%2F%2Fwww.britannica.com%2Fbiography%2FSamuel-L-Jackson&tbnid=v78IgVU6xfwm-M&vet=12ahUKEwju1brZurjrAhWol4sKHXmICisQMygEegUIARDdAQ..i&docid=fhyW195B9pPBGM&w=517&h=800&q=sameul%20jackson&ved=2ahUKEwju1brZurjrAhWol4sKHXmICisQMygEegUIARDdAQ" }
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Pic" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Tony Stark", "Male", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.sideshow.com%2Fcollectibles%2Fmarvel-iron-man-mark-vii-hot-toys-903752&psig=AOvVaw2_4dQ6HjVw4gG1_Xhh7Yrp&ust=1598517258812000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMj82_-6uOsCFQAAAAAdAAAAABAD" },
                    { 2, "Beep", "Beatrix Kiddo", "Female", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fhero.fandom.com%2Fwiki%2FBeatrix_Kiddo&psig=AOvVaw3vSoOmcvzC08qXJnFRUS7H&ust=1598517293563000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIiB5pC7uOsCFQAAAAAdAAAAABAD" },
                    { 3, "Spider Man", "Peter Parker", "Male", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.looper.com%2F162167%2Fwhat-tom-holland-was-doing-before-he-became-spider-man%2F&psig=AOvVaw0yVpntQx58N9PfkZha0uQI&ust=1598517323157000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMjVkZy7uOsCFQAAAAAdAAAAABAD" },
                    { 4, "Vincent", "Vincent Vega", "Male", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fmir-s3-cdn-cf.behance.net%2Fproject_modules%2Fdisp%2F9c90da18945163.562d202b749c9.jpg&imgrefurl=https%3A%2F%2Fwww.behance.net%2Fgallery%2F18945163%2FVincent-Vega&tbnid=iEROjo5Rfok3EM&vet=12ahUKEwjYrLOmu7jrAhVN8aYKHfYnDwEQMygCegUIARDVAQ..i&docid=_zIGG1T08wRf2M&w=600&h=400&q=vincent%20vega&ved=2ahUKEwjYrLOmu7jrAhVN8aYKHfYnDwEQMygCegUIARDVAQ" },
                    { 5, "Jules", "Jules Winnfield", "Male", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fvignette.wikia.nocookie.net%2Fquentin-tarantino%2Fimages%2F0%2F00%2F99409e.jpg%2Frevision%2Flatest%3Fcb%3D20180122000605&imgrefurl=https%3A%2F%2Fquentin-tarantino.fandom.com%2Fwiki%2FJules_Winnfield&tbnid=e7N2BZhah_0TEM&vet=12ahUKEwj5gqqxu7jrAhXNwaYKHaLWAQ4QMygAegUIARDJAQ..i&docid=9anCxwpvxKf4pM&w=615&h=864&q=jules%20winnfield&ved=2ahUKEwj5gqqxu7jrAhXNwaYKHaLWAQ4QMygAegUIARDJAQ" },
                    { 6, "Danny", "Danny Zuko", "Male", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fvignette.wikia.nocookie.net%2Fgrease%2Fimages%2Fd%2Fd5%2FDanny_Zuko.JPG%2Frevision%2Flatest%3Fcb%3D20170903200049&imgrefurl=https%3A%2F%2Fgrease.fandom.com%2Fwiki%2FDanny_Zuko&tbnid=NicdYBPZsmmL9M&vet=12ahUKEwitgpu8u7jrAhUYwMQBHUzHDpwQMygIegUIARDtAQ..i&docid=ahZYKpoRgPgp1M&w=300&h=226&q=danny%20zuko&ved=2ahUKEwitgpu8u7jrAhUYwMQBHUzHDpwQMygIegUIARDtAQ" },
                    { 7, "Warren", "Major Marquis Warren", "Male", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F51%252BoK5%252BDQwL._AC_SX466_.jpg&imgrefurl=https%3A%2F%2Fwww.amazon.com%2FHateful-Samuel-Jackson-Marquis-Warren%2Fdp%2FB07BSW21TH&tbnid=FS1zxTR1yvqZZM&vet=12ahUKEwi8w6vTu7jrAhWPWJoKHZANAPQQMygAegUIARCtAQ..i&docid=P8tpQOEtwl50-M&w=466&h=377&q=major%20marquis%20warren&ved=2ahUKEwi8w6vTu7jrAhWPWJoKHZANAPQQMygAegUIARCtAQ" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "World of the best super heroes", "Marvel univers" },
                    { 2, "World of the next best super heroes", "DC univers" },
                    { 3, "Gangsters and shit", "Taratino Univers" },
                    { 4, "Rom-Com-Musical", "Grease univers" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Pic", "Trailer" },
                values: new object[,]
                {
                    { 1, "DirectorMarvel", 1, "action", "Civil war", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fsco%2F5%2F53%2FCaptain_America_Civil_War_poster.jpg&imgrefurl=https%3A%2F%2Fsco.wikipedia.org%2Fwiki%2FFile%3ACaptain_America_Civil_War_poster.jpg&tbnid=u8kZQ76miSiUBM&vet=12ahUKEwj8toe9vLjrAhWbxsQBHRI_BpsQMygAegUIARDcAQ..i&docid=dM1vMB3Z6F7veM&w=259&h=384&q=civil%20war%20poster&ved=2ahUKEwj8toe9vLjrAhWbxsQBHRI_BpsQMygAegUIARDcAQ", "https://www.youtube.com/watch?v=FkTybqcX-Yo" },
                    { 2, "DirectorMarvel", 1, "action", "End game", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F71Z5Pu7K7bL._AC_SL1000_.jpg&imgrefurl=https%3A%2F%2Fwww.amazon.co.uk%2FAvengers-Endgame-Premium-Borderless-Various%2Fdp%2FB07P18R1KX&tbnid=H66caWGD_EcfAM&vet=12ahUKEwj77_3HvLjrAhUFBZoKHau6D0sQMygBegUIARDTAQ..i&docid=y0KEnM2o6IJr9M&w=832&h=1000&q=endgame%20poster&ved=2ahUKEwj77_3HvLjrAhUFBZoKHau6D0sQMygBegUIARDTAQ", "https://www.youtube.com/watch?v=hA6hldpSTF8" },
                    { 3, "DirectorDC", 2, "action", "Aqua man", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.amazon.com%2FAQUAMAN-MOVIE-POSTER-Sided-ORIGINAL%2Fdp%2FB07KYSLN5T&psig=AOvVaw1tUhFgoXplvaE6UQejq3PK&ust=1598517748395000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPjD-Oa8uOsCFQAAAAAdAAAAABAD", "https://www.youtube.com/watch?v=WDkg3h8PCVU" },
                    { 4, "Quentin Tarantino", 3, "action", "Pulp fiction", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.1stdibs.com%2Ffurniture%2Fwall-decorations%2Fposters%2Fpulp-fiction-original-vintage-movie-poster-american-1994%2Fid-f_14218122%2F&psig=AOvVaw1nlewPtV1kyq6qZYZ-4cmO&ust=1598517786883000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiL2YG9uOsCFQAAAAAdAAAAABAD", "https://www.youtube.com/watch?v=s7EdQ4FqbhY" },
                    { 6, "Quentin Tarantino", 3, "action", "Kill bill", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fmarkwillltc.files.wordpress.com%2F2017%2F09%2Fyjbd1jbhmpbd5ak2mw6aq95u.jpeg&imgrefurl=https%3A%2F%2Fmarkwillltc.wordpress.com%2F2017%2F09%2F08%2Fkill-bill-movie-poster-analysis%2F&tbnid=0qGM6w9ZZ4E3kM&vet=12ahUKEwjntqGTvbjrAhUGTJoKHZvnCtIQMygGegUIARDvAQ..i&docid=p5U5Bx2ypT8nHM&w=960&h=1440&q=kill%20bill%20poster&hl=en-US&ved=2ahUKEwjntqGTvbjrAhUGTJoKHZvnCtIQMygGegUIARDvAQ", "https://www.youtube.com/watch?v=7kSuas6mRpk" },
                    { 7, "Quentin Tarantino", 3, "action", "The hateful eight", "https://www.google.com/imgres?imgurl=https%3A%2F%2Ftownsquare.media%2Fsite%2F442%2Ffiles%2F2015%2F11%2FThe-Hateful-Eight-poster-preview.jpg%3Fw%3D980%26q%3D75&imgrefurl=https%3A%2F%2Fscreencrush.com%2Fhateful-eight-tv-spot-poster%2F&tbnid=vGVvMyA4pt1UsM&vet=12ahUKEwj3kISevbjrAhUKzKYKHUWGCcQQMygRegUIARDvAQ..i&docid=PPHLmlFV3F97YM&w=980&h=653&q=the%20hateful%20eight%20poster&hl=en-US&ved=2ahUKEwj3kISevbjrAhUKzKYKHUWGCcQQMygRegUIARDvAQ", "https://www.youtube.com/watch?v=nIOmotayDMY" },
                    { 5, "DirectorGreace", 4, "Musical", "Grease", "https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F51H4HIo40BL._AC_.jpg&imgrefurl=https%3A%2F%2Fwww.amazon.com%2FTravolta-Newton-John-Conaway-Stockard-Channing%2Fdp%2FB002S6SPIS&tbnid=y0tT-krpt4Bk2M&vet=12ahUKEwjZh4iKvbjrAhUFBZoKHau6D0sQMygFegUIARDoAQ..i&docid=m_xIU9l00urZMM&w=354&h=500&q=grease%20poster&hl=en-US&ved=2ahUKEwjZh4iKvbjrAhUFBZoKHau6D0sQMygFegUIARDoAQ", "https://www.youtube.com/watch?v=f2CCEixOVVU" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieId", "ActorId", "CharacterId" },
                values: new object[,]
                {
                    { 1, 2, 1 },
                    { 2, 2, 1 },
                    { 2, 3, 3 },
                    { 4, 4, 4 },
                    { 7, 5, 7 },
                    { 5, 4, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_ActorId",
                table: "MovieCharacter",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Actor");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
