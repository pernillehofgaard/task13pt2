﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Task13Pt2
{
    public class Actor
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string POB { get; set; } //Place of Birth
        public string Pic { get; set; }

        //Nav
        public ICollection<MovieCharacter> movieCharacters { get; set; } //FK

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
