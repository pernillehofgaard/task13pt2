﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Task13Pt2
{
    public class Character
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Pic { get; set; }

        public ICollection<MovieCharacter> MovieCharacter { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
