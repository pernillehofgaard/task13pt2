﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Task13Pt2
{
    public class Movie
    {
        [Key]
        [Required]
        public int Id { get; set; } //PK
        [Required]
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Pic { get; set; }
        public string Trailer { get; set; } //Youtube link

        //Nav property
        public Franchise Franchise { get; set; }
        public int FranchiseId { get; set; } //FK
        public ICollection<MovieCharacter> movieCharacters { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
