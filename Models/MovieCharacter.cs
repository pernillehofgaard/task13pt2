﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;

namespace Task13Pt2
{
    public class MovieCharacter
    {
        [Key]
        [Required]
        public int ActorId { get; set; } //FK
        [Key]
        [Required]
        public int MovieId { get; set; } //FK
        [Key]
        [Required]
        public int CharacterId { get; set; } //FK

        //Nav properties
        public Actor actor { get; set; }
        public Movie movie { get; set; }
        public Character character { get; set; }


    }
}
