﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Task13Pt2
{
    public class Franchise
    {
        [Key]
        [Required]
        public int Id { get; set; } //PK
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        
        public ICollection<Movie> movies { get; set; } //FK
    }
}
