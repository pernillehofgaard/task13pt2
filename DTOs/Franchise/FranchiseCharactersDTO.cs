﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs.Character;
using Task13Pt2.DTOs.Movie;

namespace Task13Pt2.DTOs.Franchise
{
    public class FranchiseCharactersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<MoviecharactersDto> Characters { get; set; }
    }
}
