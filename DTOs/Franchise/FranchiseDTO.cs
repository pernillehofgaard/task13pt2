﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs
{
    public class FranchiseDTO
    {
        public int Id { get; set; } //PK
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
