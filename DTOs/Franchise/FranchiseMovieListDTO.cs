﻿using System;
using System.Collections.Generic;
using Task13Pt2.DTOs.Movie;

namespace Task13Pt2.DTOs.Franchise
{
    public class FranchiseMovieListDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<MovieListDTO> MovieName { get; set; }
    }
}
