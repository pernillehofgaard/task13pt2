﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs.Franchise
{
    public class FranchiseListDTO
    {
        public int Id { get; set; } //PK
        public string Name { get; set; }
    }
}
