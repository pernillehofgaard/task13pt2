﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs
{
    public class MovieCharacterDTO
    {
        public int ActorId { get; set; } 
        public int MovieId { get; set; } 
        public int CharacterId { get; set; }
    }
}
