﻿using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs.MovieCharacter
{
    public class MovieCharacterNameDto
    {
        public int Character { get; set; }
        public int Actor { get; set; }

    }
}
