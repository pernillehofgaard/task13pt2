﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs.MovieCharacter
{
    public class MovieCharacterActorIdDto
    {
        public int ActorId { get; set; }
    }
}
