﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs.MovieCharacter;

namespace Task13Pt2.DTOs.Movie
{
    public class MoviecharactersDto
    {
        public int Id { get; set; }
        public int FranchiseId { get; set; }
        public ICollection<MovieCharacterNameDto> moviecharacters{ get; set; }
    }
}
