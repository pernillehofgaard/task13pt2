﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs
{
    public class MovieDTO
    {
        public int Id { get; set; } 
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Pic { get; set; }
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }

        public ICollection<MovieCharacterDTO> movieCharacters { get; set; }


    }
}
