﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs.Actor;
using Task13Pt2.DTOs.Character;
using Task13Pt2.DTOs.MovieCharacter;

namespace Task13Pt2.DTOs.Movie
{
    public class MovieCharacterActorListDTO
    {
        public string MovieTitle { get; set; }
        public ICollection<MovieCharacterNameDto> Character { get; set; }

    }
}
