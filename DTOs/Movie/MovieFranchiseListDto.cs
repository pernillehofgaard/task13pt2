﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13Pt2.DTOs.Movie
{
    public class MovieFranchiseListDto
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Franchise { get; set; } //Franchise name
    }
}
