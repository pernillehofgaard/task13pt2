﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs.Movie;

namespace Task13Pt2.DTOs.Actor
{
    public class ActorCharactesDto
    {
        public int Id { get; set; }
        public ICollection<MovieCharacterDTO> Characters { get; set; }
    }
}
