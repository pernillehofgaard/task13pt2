﻿using System;
using System.Collections.Generic;
using Task13Pt2.DTOs.Movie;


namespace Task13Pt2.DTOs.Character
{
    public class CharacterListDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public ICollection<MovieCharacterDTO> actors { get; set; }

    }
}
