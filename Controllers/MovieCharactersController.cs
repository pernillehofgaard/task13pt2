﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13Pt2;

namespace Task13Pt2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly DbMovieContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(DbMovieContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/MovieCharacters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacter>>> GetMovieCharacter()
        {
            return await _context.MovieCharacter.ToListAsync();
        }

        // GET: api/MovieCharacters/5/7/7
        [HttpGet("{actorId, movieId, characterId}")]
        public async Task<ActionResult<MovieCharacter>> GetMovieCharacter(int actorId, int movieId, int characterId)
        {
            var movieCharacter = await _context.MovieCharacter.FindAsync(actorId, movieId, characterId);

            if (movieCharacter == null)
            {
                return NotFound();
            }

            return movieCharacter;
        }

        // PUT: api/MovieCharacters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieCharacter(int id, MovieCharacter movieCharacter)
        {
            if (id != movieCharacter.MovieId)
            {
                return BadRequest();
            }

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieCharacters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacter>> PostMovieCharacter(MovieCharacter movieCharacter)
        {
            _context.MovieCharacter.Add(movieCharacter);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MovieCharacterExists(movieCharacter.MovieId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMovieCharacter", new { id = movieCharacter.MovieId }, movieCharacter);
        }

        // DELETE: api/MovieCharacters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieCharacter>> DeleteMovieCharacter(int id)
        {
            var movieCharacter = await _context.MovieCharacter.FindAsync(id);
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacter.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return movieCharacter;
        }

        private bool MovieCharacterExists(int id)
        {
            return _context.MovieCharacter.Any(e => e.MovieId == id);
        }
    }
}
