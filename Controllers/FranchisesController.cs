﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13Pt2.DTOs.Franchise;

namespace Task13Pt2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class FranchisesController : ControllerBase
	{
		private readonly DbMovieContext _context;
		private readonly IMapper _mapper;

		public FranchisesController(DbMovieContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		// GET: api/Franchises
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Franchise>>> GetFranchises()
		{
			return await _context.Franchises.ToListAsync();
		}

		//GET ALL MOVIES FOR A FRANCHISE
		// GET: api/Franchises/5
		[HttpGet("{id}")]
		public async Task<ActionResult<FranchiseMovieListDTO>> GetFranchise(int id)
		{
			var franchise = await _context.Franchises.Where(f => f.Id == id).Include(f => f.movies).SingleOrDefaultAsync();
		   
			if (franchise == null)
			{
				return NotFound();
			}

			FranchiseMovieListDTO dto = _mapper.Map<FranchiseMovieListDTO>(franchise);

			return dto;
		}


		//GET ALL CHARACTERS FOR A FRANCHISE
		// GET: api/Franchises/5/FranchiseCharacters
		[HttpGet("{id}/FranchiseCharacters")]
		public async Task<ActionResult<FranchiseCharactersDTO>> GetFranchiseCharacters(int id)
		{
			var franchiseMovies = await _context.Franchises.Where(fm => fm.Id == id).Include(fm => fm.movies).SingleOrDefaultAsync();

			if (franchiseMovies == null)
			{
				return NotFound();
			}

			FranchiseCharactersDTO dto = _mapper.Map<FranchiseCharactersDTO>(franchiseMovies);

			return dto;
		}
		
		

		// PUT: api/Franchises/5
		// To protect from overposting attacks, enable the specific properties you want to bind to, for
		// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
		[HttpPut("{id}")]
		public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
		{
			if (id != franchise.Id)
			{
				return BadRequest();
			}

			_context.Entry(franchise).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!FranchiseExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		// POST: api/Franchises
		// To protect from overposting attacks, enable the specific properties you want to bind to, for
		// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
		[HttpPost]
		public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
		{
			_context.Franchises.Add(franchise);
			await _context.SaveChangesAsync();

			return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
		}

		// DELETE: api/Franchises/5
		[HttpDelete("{id}")]
		public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
		{
			var franchise = await _context.Franchises.FindAsync(id);
			if (franchise == null)
			{
				return NotFound();
			}

			_context.Franchises.Remove(franchise);
			await _context.SaveChangesAsync();

			return franchise;
		}

		private bool FranchiseExists(int id)
		{
			return _context.Franchises.Any(e => e.Id == id);
		}
	}
}
