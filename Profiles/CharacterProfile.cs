﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs;
using Task13Pt2.DTOs.Character;
using Task13Pt2.DTOs.Movie;

namespace Task13Pt2.Profiles
{
	public class CharacterProfile : Profile
	{
		public CharacterProfile()
		{
			CreateMap<Character, CharacterDTO>().ReverseMap();

			CreateMap<Character, CharacterListDTO>().ForMember(cldto => cldto.actors,
				opt => opt.MapFrom(c => c.MovieCharacter));
		}
	}
}
