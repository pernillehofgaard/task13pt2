﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs;
using Task13Pt2.DTOs.Movie;
using Task13Pt2.DTOs.MovieCharacter;

namespace Task13Pt2.Profiles
{
	public class MovieProfile : Profile
	{
		public MovieProfile()
		{
			CreateMap<Movie, MovieDTO>().ReverseMap();
			CreateMap<Movie, MovieListDTO>().ReverseMap();

			CreateMap<Movie, MovieCharacterActorListDTO>().ForMember(mcaldto => mcaldto.Character,
				opt => opt.MapFrom(m => m.movieCharacters)).ReverseMap();

			CreateMap<Movie, MoviecharactersDto>()
				.ForMember(mcdto => mcdto.FranchiseId, opt => opt.MapFrom(m => m.FranchiseId))
				.ForMember(mcdto => mcdto.moviecharacters, opt => opt.MapFrom(m => m.movieCharacters));
		}
	}
}
