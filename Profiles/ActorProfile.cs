﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs;
using Task13Pt2.DTOs.Actor;
using Task13Pt2.DTOs.MovieCharacter;

namespace Task13Pt2.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDTO>().ReverseMap();
            CreateMap<Actor, ActorNameDTO>().ReverseMap();

            //characters an actor has played
            CreateMap<Actor, ActorCharactesDto>()
                .ForMember(acdto => acdto.Characters, opt => opt.MapFrom(a => a.movieCharacters));

           

        }
    }
}
