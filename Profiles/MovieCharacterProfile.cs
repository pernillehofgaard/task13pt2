﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs;
using Task13Pt2.DTOs.Character;
using Task13Pt2.DTOs.Franchise;
using Task13Pt2.DTOs.MovieCharacter;

namespace Task13Pt2.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterDTO>().ReverseMap();
            
            CreateMap<MovieCharacter, MovieCharacterNameDto>()
                .ForMember(mcldto => mcldto.Character, opt => opt.MapFrom(mc => mc.CharacterId))
                .ForMember(mcaliasdto => mcaliasdto.Actor, opt=> opt.MapFrom(mc => mc.ActorId));
            
            CreateMap<MovieCharacter, MovieCharacterActorIdDto>()
                .ForMember(mcaIdDto => mcaIdDto.ActorId, opt => opt.MapFrom(mc => mc.character.FullName));
            /*
            CreateMap<MovieCharacter, MovieCharacterNameDto>()
                .ForMember(mcndto => mcndto.Character, opt => opt.MapFrom(mc => mc.character.FullName))
                .ForMember(mcndto => mcndto.Character, opt => opt.MapFrom(mc => mc.actor.Id))
                ;
            */
            
        }
    }
}
