﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13Pt2.DTOs;
using Task13Pt2.DTOs.Franchise;

namespace Task13Pt2.Profiles
{
	public class FranchiseProfile : Profile
	{
		public FranchiseProfile()
		{
			CreateMap<Franchise, FranchiseDTO>().ReverseMap();
			CreateMap<Franchise, FranchiseListDTO>().ReverseMap();

			CreateMap<Franchise, FranchiseMovieListDTO>().ForMember(fmldto => fmldto.MovieName, 
				opt => opt.MapFrom(f => f.movies)).ReverseMap();

			CreateMap<Franchise, FranchiseCharactersDTO>()
				.ForMember(fc => fc.Id, opt => opt.MapFrom(f => f.Id))
				.ForMember(fc => fc.Name, opt => opt.MapFrom(f => f.Name))
				.ForMember(fc => fc.Characters, opt => opt.MapFrom(f => f.movies));
		}
	}
}
